"""Asynchronous server.
"""

from aiohttp import web


async def handle(_: web.Request) -> web.Response:
    """Process a request.

    Args:
        _ (web.Request): Request.

    Returns:
        web.Response: Response.
    """

    return web.Response(text="Hello world")


def main():
    """Main function.
    """

    app = web.Application()
    app.add_routes([web.get("/", handle)])
    web.run_app(app)


if __name__ == "__main__":
    main()
