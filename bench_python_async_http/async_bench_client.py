"""Client to perform benchmark.
"""

import asyncio
import datetime

import aiohttp

MAX_TASKS = 8
NUM_REQUESTS = 1000


async def request(session: aiohttp.ClientSession) -> datetime.timedelta:
    """Perform requests.
    """

    start = datetime.datetime.now()

    for _ in range(NUM_REQUESTS):
        async with session.get("http://localhost:8080") as response:
            text = await response.text()
            assert text

    return datetime.datetime.now() - start


async def bench():
    """Perform benchmark
    """

    loop = asyncio.get_event_loop()
    async with aiohttp.ClientSession() as session:
        for num_tasks_minus_one in range(MAX_TASKS):
            num_tasks = num_tasks_minus_one + 1

            task_list = []
            for _ in range(num_tasks):
                task = loop.create_task(request(session))
                task_list.append(task)

            time_sum = datetime.timedelta()
            for task in task_list:
                time = await task
                time_sum += time

            print(
                f"Average of {num_tasks} tasks: {time_sum / num_tasks}")


def main():
    """Main function.
    """

    asyncio.run(bench())


if __name__ == "__main__":
    main()
