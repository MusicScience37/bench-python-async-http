"""Client to perform benchmark.
"""

import asyncio
import datetime
import concurrent.futures

import aiohttp

MAX_TASKS = 8
NUM_REQUESTS = 1000


async def async_request() -> datetime.timedelta:
    """Perform requests asynchronously.
    """

    async with aiohttp.ClientSession() as session:
        start = datetime.datetime.now()
        for _ in range(NUM_REQUESTS):
            async with session.get("http://localhost:8080") as response:
                text = await response.text()
                assert text

    return datetime.datetime.now() - start


def request() -> datetime.timedelta:
    """Perform requests.
    """

    return asyncio.run(async_request())


def bench():
    """Perform benchmark.
    """

    pool = concurrent.futures.ProcessPoolExecutor(MAX_TASKS)

    for num_tasks_minus_one in range(MAX_TASKS):
        num_tasks = num_tasks_minus_one + 1

        future_list = []
        for _ in range(num_tasks):
            future = pool.submit(request)
            future_list.append(future)

        time_sum = datetime.timedelta()
        for future in future_list:
            time = future.result()
            time_sum += time

        print(f"Average of {num_tasks} tasks: {time_sum / num_tasks}")


def main():
    """Main function.
    """

    bench()


if __name__ == "__main__":
    main()
